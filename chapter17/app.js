/**
 * Create an XMLHttpRequest Object
 * @returns {XMLHttpRequest}
 */
var getHttpRequest = function () {
  var request = false

  if (window.XMLHttpRequest) {
    //Firefox, Opera, IE7, and other browsers will use the native object
    var request = new XMLHttpRequest()
    // if (request.overrideMimeType) {
    //   request.overrideMimeType('text/xml')
    // }
  } else if (window.ActiveXObject) {
    try {
      //IE 5 and 6 will use the ActiveX control
      var request = new ActiveXObject('Msxml2.XMLHTTP')
    } catch (error) {
      try {
        request = new ActiveXObject('Microsoft.XMLHTTP')
      } catch (error) {}
    }
  }
  if (!request) {
    console.log('Impossible to create an XMLHTTP Request Object')
    return false
  }
  return request
} // GET requests
// (function () {
var links = document.querySelectorAll('a.meteo')
var result = document.getElementById('result')
for (var i = 0; i < links.length; i++) {
  var link = links[i]
  link.addEventListener('click', function (e) {
    e.preventDefault()
    result.innerHTML = 'Loading...'

    // create the request object
    var httpRequest = getHttpRequest()
    // callback functions for request
    httpRequest.onreadystatechange = function () {
      if (httpRequest.readyState === 4) {
        result.innerHTML = ''
        if (httpRequest.status === 200) {
          // result.innerText = this.responseText
          /** @type {Array} */
          var results = JSON.parse(this.responseText)

          var ul = document.createElement('ul')
          result.appendChild(ul)
          for (var i = 0; i < results.length; i++) {
            var li = document.createElement('li')
            li.innerHTML = results[i].name
            ul.appendChild(li)
          }
        } else {
          alert('Could not contact the server')
        }
      }
    }
    // open the connection
    // httpRequest.open('GET', this.getAttribute('href'), true)

    // httpRequest.overrideMimeType('text/plain')
    // httpRequest.setRequestHeader('Cache-Control', 'no-cache')
    // httpRequest.setRequestHeader('Content-Type', 'text/plain')

    httpRequest.open('GET', 'https://jsonplaceholder.typicode.com/users', true)
    httpRequest.setRequestHeader('Cache-Control', 'no-cache')
    httpRequest.setRequestHeader('Content-Type', 'application/json')

    // httpRequest.open('GET', 'http://localhost:8000/zszssdz')
    // httpRequest.setRequestHeader('Content-Type', 'application/json')
    // send the request
    httpRequest.send()
  })
}
// })() // POST request
// ;(function () {
var links = document.querySelectorAll('a.post-demo')
var result = document.getElementById('result')
for (var i = 0; i < links.length; i++) {
  var link = links[i]
  link.addEventListener('click', function (e) {
    e.preventDefault()
    result.innerHTML = 'Loading...'

    // create the request object
    var httpRequest = getHttpRequest()
    // callback functions for request
    httpRequest.onreadystatechange = function () {
      if (httpRequest.readyState === 4) {
        result.innerHTML = ''
        if (httpRequest.status === 200) {
          // result.innerText = this.responseText
          result.innerHTML = httpRequest.responseText
        } else {
          alert('Could not contact the server')
        }
      }
    }
    // open the connection
    httpRequest.open('POST', 'http://localhost:8000/demo2.php', true)

    var caracteres_speciaux = 'a&fgv-èjghjµ=^lùêgf'

    /*
  
  IE 9 and lower
  httpRequest.setRequestHeader('Cache-Control', 'no-cache')
    httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
    // send the request
  httpRequest.send(
      'city=' +
        encodeURIComponent('Montpellier') +
        '&name=' +
        encodeURIComponent('Sally Sparrow') +
        '&caracteres_speciaux=' +
        encodeURIComponent(caracteres_speciaux)
    )*/
    var data = new FormData()
    data.append('city', 'Montpellier')
    data.append('name', 'Sally Sparrow')
    data.append('caracteres_speciaux', caracteres_speciaux)
    httpRequest.send(data)
  })
}

// })

// form test
var result = document.getElementById('result')
/** @type {HTMLFormElement} */
var form = document.getElementById('form')
form.addEventListener('submit', function (e) {
  e.preventDefault()
  result.innerHTML = 'Loading...'

  // create the request object
  var httpRequest = getHttpRequest()
  // callback functions for request
  httpRequest.onreadystatechange = function () {
    if (httpRequest.readyState === 4) {
      result.innerHTML = ''
      if (httpRequest.status === 200) {
        // result.innerText = this.responseText
        result.innerHTML = httpRequest.responseText
      } else {
        alert('Could not contact the server')
      }
    }
  }
  // open the connection
  httpRequest.open('POST', 'http://localhost:8000/demo2.php', true)

  var data = new FormData(form)
  httpRequest.send(data)
})
