<?php

use App\Validator;

require '_inc.php';

function isAjax()
{
    return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

$_SESSION = [];


$errors = [];
$emails = ["contact@local.dev", "depannage@local.dev", "heimerdinger@local.dev"];


$validator = new Validator($_POST);
$validator->check('name', 'required');
$validator->check('email', 'required');
$validator->check('email', 'email');
$validator->check('message', 'required');
$validator->check('message', 'in', array_keys($emails));
$errors = $validator->getErrors();

if (!empty($errors)) {
    if (isAjax()) {
        echo json_encode($errors);
        header('Content-Type: application/json');
        http_response_code(400);
        die();
    }

    $_SESSION['errors'] = $errors;
    $_SESSION['inputs']['name'] = $_POST['name'];
    $_SESSION['inputs']['email'] = $_POST['email'];
    $_SESSION['inputs']['service'] = $_POST['service'];
    $_SESSION['inputs']['message'] = $_POST['message'];
} else {
    $message = $_POST['message'];
    $to = $emails[$_POST['service']];
    $headers = "From: site@local.dev";
    $response = mail($to, 'Formulaire de contact', $message, $headers);

    if (isAjax()) {
        echo json_encode(['success' => true, 'message' => 'Votre email a bien été envoyé']);
        header('Content-Type: application/json');
        die();
    }
    $_SESSION['success'] = true;
}

header('Location: index.php');
