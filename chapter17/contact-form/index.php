<?php require '_inc.php'; ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <title>Formulaire de contact</title>
    <style>
        body {
            padding-top: 5rem;
        }

        .starter-template {
            padding: 3rem 1.5rem;
            /* text-align: center; */
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="#">Contact</a>
    </nav>

    <main role="main" class="container starter-template">
        <?php if (array_key_exists('errors', $_SESSION)) : ?>
            <div class="alert alert-danger">
                <?= implode('<br>', $_SESSION['errors']); ?>
            </div>
        <?php endif; ?>
        <?php if (array_key_exists('success', $_SESSION)) : ?>
            <div class="alert alert-success">
                Votre email a bien été envoyé
            </div>
        <?php endif; ?>

        <form action="post_contact.php" method="post" id="contact">
            <?php $form = new \App\Form(isset($_SESSION['inputs']) ? $_SESSION['inputs'] : []); ?>
            <div class="form-row">

                <?= $form->text('name', 'Votre nom', 'col-md-4', 'required'); ?>
                <?= $form->email('email', 'Votre email', 'col-md-4', 'required'); ?>
                <?= $form->select('service', 'Service', ['Contact', 'Dépannage', 'Heimerdinger'], 'col-md-4'); ?>
            </div>
            <?= $form->textarea('message', 'Votre message', '', 'required'); ?>
            <!-- <?= $form->text('thesurname', '', '', 'required'); ?> -->

            <?= $form->submit('Envoyer le message'); ?>
            <!-- </div> -->
        </form>
        <!-- </div> -->
    </main>
    <?php // var_dump($_SESSION);
    ?>
    <script src="app.js"></script>
</body>

</html>
<?php
unset($_SESSION['errors'], $_SESSION['inputs'], $_SESSION['success']);
