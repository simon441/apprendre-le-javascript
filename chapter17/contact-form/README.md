Formulaire de contact en PHP 

Dans ce tutoriel vidéo vous apprendrez à coder un formulaire de contact qui soit utilisable. Dans un premier temps, nous verrons la création d'un formulaire XHTML permettant à l'internaute la saisie des informations. Dans un deuxième temps, nous verrons le traitement de ces informations via un script PHP qui enverra le contenu du formulaire par mail.

https://www.grafikart.fr/tutoriels/formulaire-php-21
