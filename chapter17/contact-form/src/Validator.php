<?php

namespace App;

class Validator
{
    private $data = [];

    private $errors = [];

    private $errorMessages = [
        ""
    ];

    /**
     * __construct
     *
     * @param  array $data
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function check(string $name, string $rule, $options = null)
    {
        $validator = "validate" . ucfirst($rule);
        if (!$this->$validator($name, $options)) {
            $this->errors[$name] = "Le champ $name n'a pas été rempli correctement"; //$this->errorMessages[$name];
        }
    }

    public function validateRequired(string $name): bool
    {
        return array_key_exists($name, $this->data) && !empty($this->data[$name]);
    }

    public function validateEmail(string $name): bool
    {
        return array_key_exists($name, $this->data) && filter_var($this->data[$name], FILTER_VALIDATE_EMAIL);
    }

    public function validateIn(string $name, array $options): bool
    {
        return array_key_exists($name, $this->data) && in_array($name, $options);
    }

    /**
     * 
     * Get the value of errors
     * return array
     */ 
    public function getErrors(): array
    {
        return $this->errors;
    }
}
