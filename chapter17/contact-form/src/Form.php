<?php

namespace App;

class Form
{
    private $data = [];

    /**
     * __construct
     *
     * @param  array $data
     * @return void
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * text
     *
     * @param  string $name
     * @param  string $label
     * @param  string $class
     * @return string
     */
    public function hidden(string $name, $attributes = null): string
    {
        $value = $this->getValue($name);

        $attributes = is_array($attributes) ? '"' . implode('" "', $attributes) . '" ' : $attributes;
        return "<input type=\"hidden\" class=\"form-control\"  name=\"$name\" id=\"input{$name}\" value=\"$value\" $attributes>";
    }

    /**
     * text
     *
     * @param  string $name
     * @param  string $label
     * @param  string $class
     * @return string
     */
    public function text(string $name, string $label, string $class = '', $attributes = null): string
    {
        return $this->input('text', $name, $label, $class, $attributes);
    }

    /**
     * email
     *
     * @param  string $name
     * @param  string $label
     * @param  string $class
     * @return string
     */
    public function email(string $name, string $label, string $class = '', $attributes = null): string
    {
        return $this->input('email', $name, $label, $class, $attributes);
    }

    /**
     * textarea
     *
     * @param  string $name
     * @param  string $label
     * @param  string $class
     * @return string
     */
    public function textarea(string $name, string $label, string $class = '', $attributes = null): string
    {
        return $this->input('textarea', $name, $label, $class, $attributes);
    }

    /**
     * select
     *
     * @param  string $name
     * @param  string $label
     * @param  string $options
     * @param  string $class
     * @return string
     */
    public function select(string $name, string $label, array $options, string $class = '', $attributes = null): string
    {
        $value = $this->getValue($name);

        $optionsHtml = '';

        foreach ($options as $k => $v) {
            $selected = '';
            if ($value == $k) {
                $selected = ' selected ';
            }
            $optionsHtml .= "<option value=\"$k\" $selected>$v</option>";
        }
        $attributes = is_array($attributes) ? '"' . implode('" "', $attributes) . '" ' : $attributes;
        return <<< HTML
        <div class="form-group {$class}">
            <label for="input{$name}">$label</label>
            <select class="form-control" name="{$name}" id="input{$name}" $attributes>
                $optionsHtml
            </select>    
    </div>
HTML;
    }

    /**
     * submit
     *
     * @param  string $label
     * @return string
     */
    public function submit(string $label): string
    {
        return "<button type=\"submit\" class=\"btn btn-primary\">{$label}</button>";
    }

    /**
     * input
     *
     * @param  string $type
     * @param  string $name
     * @param  string $label
     * @param  string $class
     * @return string
     */
    private function input(string $type, string $name, string $label, string $class = '', $attributes = null): string
    {
        $value = $this->getValue($name);

        $attributes = is_array($attributes) ? '"' . implode('" "', $attributes) . '" ' : $attributes;

        if ($type === 'textarea') {
            $input = "<textarea class=\"form-control\"  name=\"message\" id=\"input{$name}\" $attributes>{$value}</textarea>";
        } else {
            $input = "<input type=\"{$type}\" class=\"form-control\"  name=\"$name\" id=\"input{$name}\" $attributes value=\"$value\">";
        }
        return <<<EOT
        <div class="form-group {$class}">
        <label for="input{$name}">$label</label>
        $input
        </div>
EOT;
    }

    /**
     * getValue
     *
     * @param  string $name
     * @return mixed
     */
    private function getValue(string $name)
    {
        $value = "";
        if (isset($this->data[$name])) {
            $value = $this->data[$name];
        }
        return $value;
    }
}
