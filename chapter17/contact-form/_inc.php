<?php
session_start();

require __DIR__ . "/vendor/autoload.php";

function dd(...$value) {
    echo '<pre>';
    var_dump($value);
    echo '</pre>';
    exit();
}
