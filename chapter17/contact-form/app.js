/**
 * Get an XMLHttpRequest Object
 * @returns {XMLHttpRequest}
 */
var getHttpRequest = function () {
  var request = false

  if (window.XMLHttpRequest) {
    //Firefox, Opera, IE7, and other browsers will use the native object
    var request = new XMLHttpRequest()
    // if (request.overrideMimeType) {
    //   request.overrideMimeType('text/xml')
    // }
  } else if (window.ActiveXObject) {
    try {
      //IE 5 and 6 will use the ActiveX control
      var request = new ActiveXObject('Msxml2.XMLHTTP')
    } catch (error) {
      try {
        request = new ActiveXObject('Microsoft.XMLHTTP')
      } catch (error) {}
    }
  }
  if (!request) {
    console.log('Impossible to create an XMLHTTP Request Object')
    return false
  }
  return request
}

/**
 * Send a Form via an AJAX POST request
 * @param {HTMLFormElement} form  the selected form
 * @param {HTMLButtonElement} button  the button to submit the form
 */
var sendAjaxForm = function (form, button) {
  /** @type {string} text of the wsubmit button */
  var buttonText = button.textContent ? button.textContent : button.innerText

  form.addEventListener('submit', function (e) {
    // disable the button
    button.disabled = true
    if (button.textContent) {
      button.textContent = 'Loading...'
    } else {
      button.innerText = 'Loading...'
    }

    // empty errors messages
    var errorElements = form.querySelectorAll('.is-invalid')
    for (var i = 0; i < errorElements.length; i++) {
      errorElements[i].classList.remove('is-invalid')
      var span = errorElements[i].parentNode.querySelector('.invalid-feedback')
      if (span !== null) {
        span.parentNode.removeChild(span)
      }
    }

    e.preventDefault()

    // get form data
    var data = new FormData(form)

    // AJAX request
    var xhr = getHttpRequest()

    // listen to xhr events
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
        if (xhr.status !== 200) {
          var errors = JSON.parse(xhr.responseText)
          var errorKeys = Object.keys(errors)
          for (var i = 0; i < errorKeys.length; i++) {
            var key = errorKeys[i]
            var error = errors[key]

            // input
            var input = document.querySelector('[name=' + key + ']')
            input.classList.add('is-invalid')

            // error span
            var span = document.createElement('span')
            span.className = 'invalid-feedback'
            span.innerHTML = error
            // add span to input
            input.parentNode.appendChild(span)
          }
        } else {
          var response = JSON.parse(this.responseText)
          console.log(response)
          var message = document.createElement('div')
          message.className = 'alert alert-success'
          message.innerHTML = response.message
          form.parentNode.insertBefore(message, form)
          var inputs = form.querySelectorAll('input, textarea')
          for (var i = 0; i < inputs.length; i++) {
            inputs[i].value = ''
          }
        }

        // enable the button again
        button.disabled = false
        if (button.textContent) {
          button.textContent = buttonText
        } else {
          button.innerText = buttonText
        }
      }
    }

    // open and send request
    xhr.open('POST', form.getAttribute('action'), true)
    xhr.setRequestHeader('X-Requested-With', 'xmlhttprequest')
    xhr.send(data)
  })
}

/** @type {HTMLFontElement} the selected contact form */
var form = document.querySelector('#contact')
/** @type {HTMLButtonElement} */
var button = form.querySelector('button[type=submit]')
sendAjaxForm(form, button)
