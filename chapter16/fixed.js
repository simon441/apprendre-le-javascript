;(function () {
  var scrollY = function () {
    var supportPageOffset = window.pageXOffset !== undefined
    var isCSS1Compat = (document.compatMode || '') === 'CSS1Compat'

    // var x = supportPageOffset ? window.pageXOffset : isCSS1Compat ? document.documentElement.scrollLeft : document.body.scrollLeft
    return supportPageOffset ? window.pageYOffset : isCSS1Compat ? document.documentElement.scrollTop : document.body.scrollTop
  }
  /*
    LORSQUE L'ON scrolle
        SI le menu sort de l'écran ALORS
            il deviendra fixe
    */

  // variables
  /** @type {HTMLDivElement[]} */
  var elements = document.querySelectorAll('[data-sticky]')

  // functions
  /**
   * Make a sticky element
   * @param {HTMLElement} element Element to make sticky
   * Options: data-sticky: make element sticky
   *          data-offset="number": (number is in pixels): offset from the top when sticky element scrolls away;
   *          data-constraint="constraint-element": (constraint-element can be any CSS selector) : the constraint is an element which the selected element must not get out off
   */
  makeSticky = function (element) {
    var rect = element.getBoundingClientRect()
    /** @type {number} offset from the top when the element is in fixed positioning */
    var offset = parseInt(element.getAttribute('data-offset') || 0, 10)

    // constraint
    /** @type {string} Constraint to an element which it cannot get out of */
    if (element.hasAttribute('data-constraint')) {
      /** @type {HTMLElement} constraint for the fixed element */
      var constraint = document.querySelector(element.getAttribute('data-constraint'))
    } else {
      var constraint = document.body
    }
    var constraintRect = constraint.getBoundingClientRect()
    /** @type {number} Where is the bottom position of the constraint? */
    var constraintBottom = constraintRect.top + scrollY() + constraintRect.height

    /** @type {number} element top at start*/
    var top = rect.top + scrollY()

    /** @type {HTMLDivElement} Fake div to take the place of the original element */
    var fakeElement = document.createElement('div')
    fakeElement.style.width = rect.width + 'px'
    fakeElement.style.height = rect.height + 'px'

    // functions
    /**
     * Actions on scroll
     * @param {EventTarget} e
     */
    var onScroll = function (e) {
      if (scrollY() > constraintBottom && element.style.position != 'absolute') {
        // is the element bottom at this time under the bottom of the constraint and not absolute?
        element.classList.remove('fixed')
        element.style.position = 'absolute'
        element.style.bottom = '0'
        element.style.top = 'auto'
      } else if (scrollY() > top - offset && scrollY() < constraintBottom && element.style.position != 'fixed') {
        //element to fixed if scroll is enough, not already fixed and not over the constraint bottom: position fixed and add fake div
        element.classList.add('fixed')
        element.style.position = 'fixed'
        element.style.top = offset + 'px'
        element.style.bottom = 'auto'
        element.style.width = rect.width + 'px'
        element.parentNode.insertBefore(fakeElement, element)
      } else if (scrollY() < top - offset && element.style.position != 'static') {
        // element back to normal: static and remove fake div
        element.classList.remove('fixed')
        element.style.position = 'static'
        // remove fake div if exists
        if (element.parentNode.contains(fakeElement)) {
          element.parentNode.removeChild(fakeElement)
        }
      }
    }

    /**
     * Actions on Window resize
     * Recalculate everything, reposition and redisplay
     * @param {FocusEvent} e
     */
    var onResize = function (e) {
      // remove styling
      element.style.width = 'auto'
      element.classList.remove('fixed')
      element.style.position = 'static'
      fakeElement.style.display = 'none'

      // calculate
      constraintRect = constraint.getBoundingClientRect()
      constraintBottom = constraintRect.top + scrollY() + constraintRect.height
      rect = element.getBoundingClientRect()
      top = rect.top + scrollY()

      // reapply styling
      fakeElement.style.width = rect.width + 'px'
      fakeElement.style.height = rect.height + 'px'
      fakeElement.style.display = 'block'

      // redisplay
      onScroll()
    }

    // Listeners
    window.addEventListener('scroll', onScroll)

    window.addEventListener('resize', onResize)

    // declare the makeSticky function to all
    window.makeSticky = makeSticky
  }

  // loop through the elements to make the all sticky
  for (var i = 0; i < elements.length; i++) {
    makeSticky(elements[i])
  }
})()
