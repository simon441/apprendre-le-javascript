// fonctions
/*
FUNCTION nom de la fonction (paramètres d'entrée séparés par une virgule){
  RETURN valeur de retour
}

Pour appeler une fonction: nom_de_la_fonction(arguments demandés si besoin)

Si on ne passe pas un des paramètres, le paramètre aura automatiquement la valeur undefined
*/
var estPremier = function (nombre) {
  for (var i = 2; i < nombre; i++) {
    if (nombre % i === 0) {
      return false
    }
  }
  return true
}

console.log(estPremier(21))

function multiplie(a, b) {
  if (b === undefined) {
    b = 1
  }
  return a * b
}

console.log(multiplie(2, 4))
console.log(multiplie(2))

/**
 * EXERCICE
 * function pour afficher qui a la mmoyenne avec comme nom: afficheQuiALaMoyenne
 * paramètres: eleves
 * affiche untel a la moyenne
 */

var classeA = [
  {
    nom: 'Marc',
    moyenne: 15,
  },
  {
    nom: 'Marion',
    moyenne: 10,
  },
  {
    nom: 'Antoine',
    moyenne: 4,
  },
  {
    nom: 'Sophia',
    moyenne: 14,
  },
]

var classeB = [
  {
    nom: 'Sébastien',
    moyenne: 8,
  },
  {
    nom: 'Miyu',
    moyenne: 6,
  },
  {
    nom: 'Pedro',
    moyenne: 14,
  },
  {
    nom: 'Violette',
    moyenne: 14,
  },
]

function afficheQuiALaMoyenne(eleves) {
  for (i = 0; i < eleves.length; i++) {
    var eleve = eleves[i]
    if (eleve.moyenne >= 10) {
      console.log(eleve.nom + ' a la moyenne')
    }
  }
}

afficheQuiALaMoyenne(classeA)
afficheQuiALaMoyenne(classeB)

// parseInt pour convertir en entier, parseFloat pour convertir en float
// conversion d'une chaîne de caractères en nombre. la conversion s'arrête au premier caractère non numerique
// parseInt(valeur à convertir, base); la base est de base 2 à ...
// parseFloat() est similaire

var pikachu = {
  crier: function () {
    console.log('PIKACHU')
  },
}

pikachu.crier()

// utilisation de la fonction push des tableaux
var aLaMoyenne = function (eleves) {
  var moyennes = []
  for (var i = 0; i < eleves.length; i++) {
    var eleve = eleves[i]
    if (eleve.moyenne >= 10) {
      moyennes.push(eleve)
    }
  }
  return moyennes
}

aLaMoyenne(classeA)

/*
EXERCICE
combien de fois chaque mot est présent dans la phrase suivante
*/
var phrase =
  "Le premier point auquel il faut faire attention est le choix de la propriété à modifier pour un élément HTML. Toutes les propriétés n'ont pas le même impact lors du rendu de la page. Par exemple si on modifie la propriété"
/*
ON SEPARE notre phrase GRACE AUX ESPACES (split)
ON CREE un compteur
POUR CHAQUE mots
  SI le mot est dans mon compteur ALORS (compteur[mot])
    Je l'incrémente
  SINON
    Je lui assigne la valeur un
AFFICHE compteur
*/

var frequence = function (phrase) {
  var mots = phrase.toLowerCase().split(' ')
  compteur = {}
  for (i = 0; i < mots.length; i++) {
    var mot = mots[i]
    if (compteur[mot] !== undefined) {
      compteur[mot]++
    } else {
      compteur[mot] = 1
    }
  }
  return compteur
}

console.log(frequence(phrase))

/*
EXERCICE
quel élève est meilleur que l'autre
estMeilleur(eleve1, eleve2)
Renvoye true si eleve1 a une meilleur moyenne que élève2
Vous pousez créer une fonction moyenne qui prend en paramètre un élève et retourne la moyenne de cet élève
 */
var eleve1 = {
  nom: 'Jean',
  note: [15, 16, 18, 2],
}
var eleve2 = {
  nom: 'Sophia',
  note: [5, 20, 18, 18],
}

var moyenne = function (notes) {
  var somme = 0
  for (i = 0; i < notes.length; i++) {
    somme += notes[i]
  }
  return somme / notes.length
}
var estMeilleur = function (a, b) {
  return moyenne(a.note) > moyenne(b.note)
}

console.log(estMeilleur(eleve1, eleve2))
