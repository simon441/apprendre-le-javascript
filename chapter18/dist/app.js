/***************************
 STRING MANIPULATION 
***************************/

var demo = '    aze  '
console.log(_.trim(demo)) // result: aze

var demoNumber = '000420000'
console.log(_.trim(demoNumber, '0')) // result: "42"

console.log(_.pad('14', 6)) // result: "  14  "
console.log(_.padStart('14', 6, '0')) // result: 000014

var name = 'john doe'
console.log(_.capitalize(name)) // result: "John doe"
console.log(_.camelCase(name)) // result: "johnDoe"
console.log(_.kebabCase(name)) // result: "john-doe"
console.log(_.upperCase(name)) // result: "JOHN DOE"
console.log(_.upperCase('__name__')) // result: "NAME"
console.log(_.kebabCase('je suis une URL intéressante numéro 42')) // result: "je-suis-une-url-interessante-numero-42"

/***************************
 * ARRAY MANIPULATION
 ***************************/
var tab = [1, 2, 3]
var tab2 = [4, 5]
console.log(_.concat(tab, tab2, 6, 7, 9, [42])) // result: Array(9) [ 1, 2, 3, 4, 5, 6, 7, 9, 42 ]
console.log(_.without(tab, 2)) // result: Array [ 1, 3 ]
var comment = { username: 'Sally', content: 'Salut les gens!' }
var tabComment = [1, comment, 3]
console.log(_.without(tabComment, comment)) // result: Array [ 1, 3 ]
console.log(
  _.filter(tab, function (element) {
    return element > 1
  })
) // result: Array [ 2, 3 ])

/***************************
 * OBJECT MANIPULATION
 ***************************/

var users = [
  { user: 'fred', age: 40, active: false },
  { user: 'barney', age: 36, active: true },
  { user: 'pebbles', age: 1, active: true },
  { user: 'garfield', age: 10, active: false },
  { user: 'timmy', age: 4, active: true },
]

// _.filter: filter by a criteria

// the following two expressions are equivalent
console.log(
  _.filter(users, function (user) {
    return user.active
  })
) /*
result: 
  Array ({ user: 'barney', age: 36, active: true,},
  { user: 'pebbles', age: 1, active: true,},
  { user: 'timmy', age: 4, active: true,})
equivalent to
  */

console.log(_.filter(users, { active: false })) // result Array [ Object { user: "fred", age: 40, active: false }, Object { user: "garfield", age: 10, active: false }]

// find: find the first
console.log(_.find(users, { active: false })) // result: Object { user: "fred", age: 40, active: false }

// order by
console.log(_.orderBy(users, ['user', 'age'], ['asc', 'desc']))
/*
result: [ 
  { "user": "barney", "age": 36, "active": true},
  { "user": "fred", "age": 40, "active": false  },
  { "user": "garfield", "age": 10,² "active": false  },
  { "user": "pebbles","age": 1,"active": true},
  { "user": "timmy","age": 4,"active": true}
]
*/

// map

console.log(
  _.map(users, function (user) {
    return user.age
  })
) // result: [(40,36,1,10,4)]

console.log(
  _.map(users, function (user) {
    user.age *= 2
    return user
  })
) /*
 result: [ {"user": "fred","age": 80,"active": false},
  {"user": "barney","age": 72,"active": true},
  {"user": "pebbles","age": 2,"active": true},
  {"user": "garfield","age": 20,"active": false},
  {"user": "timmy","age": 8,"active": true}
]
*/

// for each
_.forEach(users, function (value, key) {
  console.log(key, '=>', value)
})
/*
result:
0 => Object { user: "fred", age: 80, active: false }
1 => Object { user: "barney", age: 72, active: true }
2 => Object { user: "pebbles", age: 2, active: true }
3 => Object { user: "garfield", age: 20, active: false }
​*/

_.forEach(users[0], function (value, key) {
  console.log(key, '=>', value)
})
/* 
result:
user => fred app.js:117:11
age => 80 app.js:117:11
active => false app.js:117:11
*/

// récupérer des utilisateurs au hasard
console.log(_.sample(users)) // un utilisateur
console.log(_.sampleSize(users, 2)) // deux utilisateurs

// group by
console.log(
  _.groupBy(users, function (user) {
    return user.user.substr(0, 1)
  })
) /*
get the users with the key is the first letter of thrir name
result:
{ "f": [ { "user": "fred", "age": 80, "active": false }],
  "b": [ { "user": "barney", "age": 72, "active": true } ],
  "p": [ { "user": "pebbles", "age": 2, "active": true }],
  "g": [ { "user": "garfield", "age": 20, "active": false }],
  "t": [ { "user": "timmy", "age": 8,  "active": true }]
}
*/

// taille de l'objet
var user = users[0]
console.log(Object.keys(user).length) // vanilla js-> result: 3
console.log(_.size(user)) // lo dash -> result: 3

// gestion des objets: copier un objet puis le modifier sans changer l'objet de base: methode _.clone()
var user2 = _.clone(user)
user2.age = user2.age * 3
console.log(user) // result: Object { user: "fred", age: 80, active: false }
console.log(user2) // result: Object { user: "fred", age: 240, active: false }

_.assign(user2, { active: true, age: 2 }) // result: Object { user: "fred", age: 2, active: true }
var user2 = _.assign({}, user, { active: false, age: 42 }) // result: Object { user: "fred", age: 42, active: false }

// _.unset: supprimer une clé
_.unset(user2, 'age')
console.log(user2) // result: Object { user: "fred", active: false }
var user2 = _.assign({}, user, { user: { firstname: 'John', lastname: 'Doe' }, active: false, age: 42 })
_.unset(user2, 'user.firstname')
console.log(user2) // result: { "user": { "lastname": "Doe" }, "age": 42, "active": false }
_.set(user2, 'user.firstname', 'Simo')
console.log(user2) // result: { "user": { "lastname": "Doe", "firstname": "Simo" }, "age": 42, "active": false }

// _.has(object, 'property') tester si une propriété existe dans un objet
// _.get(object, 'property', defaultValue) récupère une propriété si elle existe dans un objet sinon renvoie defaultValue
if (_.has(user2, 'user.firstname') && user2.user.firstname == 'demo') {
  console.log('ok')
}
// equivalent à
if (_.get(user2, 'user.firstname', false) === 'demo') {
  console.log('ok')
}

/************************
 * DEBOUNCE et THROTTLE *
 * debounce lance toutes les x secondes/ms
 * throttle toutes les x ms tant qu'il y a un évènement tu continue (ex: tant qu'il y a du scroll tu scroll)
 ************************/

window.addEventListener(
  'scroll',
  _.debounce(function () {
    console.log('je scrolle avec debounce')
  }, 200)
)

window.addEventListener(
  'scroll',
  _.throttle(function () {
    console.log('je scrolle avec throttle')
  }, 200)
)

window.addEventListener(
  'scroll',
  _.throttle(function () {
    console.log('je scrolle sans rien')
  }, 200)
)
