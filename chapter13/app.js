// Les évènements
// var p = document.querySelector('p') // sélection du premier paragraphe
// var rougit = function () {
//   p.classList.toggle('red')
// }

// p.addEventListener('click', rougit)

// var ps = document.querySelectorAll('p')
// for (var i = 0; i < ps.length; i++) {
//   var p = ps[i]

//   p.addEventListener('mouseover', function () {
//     this.classList.add('blue')
//   })
//   p.addEventListener('mouseout', function () {
//     this.classList.remove('blue')
//   })
// }
var liens = document.querySelectorAll('a.external')
for (var i = 0; i < liens.length; i++) {
  var lien = liens[i]
  lien.addEventListener('click', function (e) {
    e.stopPropagation() // évite que les évènements enfants qui envoient le click agissent sur le parent
    console.log("J'ai cliqué sur le lien", e)
    var reponse = window.confirm('Voulez-vous vraiment quitter le site?')
    if (reponse === false) {
      e.preventDefault()
    }
  })

  //   var p = document.querySelector('p')
  //   var onClick = function (e) {
  //     this.classList.add('red')

  //     e.preventDefault()
  //     console.log(e)
  //     console.log("J'ai cliqué sur le paragraphe", e)
  //     p.removeEventListener('click', onClick)
  //   }

  //   p.addEventListener('click', onClick)
  /** champs et formulaires */
}
