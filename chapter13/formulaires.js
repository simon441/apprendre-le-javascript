document.querySelector('#form').addEventListener('submit', function (e) {
  console.log(e)
  var cp = document.querySelector('#cp')
  if (cp.value.length !== 5) {
    alert("Le code postal n'est pas bon")
    e.preventDefault()
  }
  var mentions = document.querySelector('#mentions')
  if (!mentions.checked) {
    alert("Vous n'avez pas accepté les CGU")
    e.preventDefault()
  }
  var age = parseInt(document.querySelector('#age').selectedOptions[0].value, 10)
  if (age < 18) {
    alert('Vous ne pouvez pas renter')
    e.preventDefault()
  }
})

document.addEventListener('DOMContentLoaded', function () {
  var demo = document.querySelector('#demo')
  console.log(demo.value)
})
