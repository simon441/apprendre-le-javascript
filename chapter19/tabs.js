/*
// vanilla JS
var links = document.querySelectorAll('.tabs a')
for (var i = 0; i < links.length; i++) {
  var link = links[i]
  link.style.color = '#f00'
}
*/

// // changer la couleur de tous les liens de la classe tab
// $('.tabs a').css('color', '#f00')

// // sélectionner le premier élément
// // valeur courante d'un attribut : attr(nomAttribut)
// console.log($('.tabs a:first').attr('href'))
// // changer la valeur d'un attribut : attr(nomAttribut, nouvelleValeur)
// console.log($('.tabs a:first').attr('href', 'AZEAZE'))

// // ajouter une classe
// // console.log($('.tabs li a:first').addClass('active'))

// // élément parent
// console.log($('.tabs li a:first').parent())
// // éléments frères et soeurs,tous les éléments au même niveau
// console.log($('.tabs li:first').siblings())

// /*
// // rajouter un li au premier ul
// // var ul = $('ul:first')
// // var li = $('<li class="active"></li>')
// // var a = $('<a href="#">Salut</a>')
// // li.append(a)
// // ul.append(li) // ajoute après
// // ul.prepend(li) // ajoute avant
// */

// // rajouter un li après le premier
// var firstLi = $('li:first')
// var li = $('<li class="active"></li>')
// var a = $('<a href="#">Salut</a>')
// li.append(a)
// firstLi.after(li) // ajoute après
// firstLi.before(li) // ajoute avant

// // modifier le contenu du premier a
// var a_ali = $('a:first')
// a_ali.text('Salut le gens') // le contenu inséré est du texte brut
// a_ali.html('<span><strong>Salut les gens</strong></span>') // le contenu inséré est du HTML
// console.log(a_ali.text()) // retourne du texte
// console.log(a_ali.html()) // retourne du HTML

// /*
// NB: pour convertir un élément Vanilla JS en JQuery,
// pour un élément 'el' faire $(el) le transforme en objet jquery
// ex: dans une fonction anonyme d'évènement: this faire référence à this de JS, pour le convertir faire: $(this)
// */
// /*
// EVENEMENTS:
// méthode 1: $(selector).on('eventType', function(e) {})
// méthode 2: $(selector).eventType(function(e) {})
// */
// $('.tabs a').on('click', function (e) {
//   console.log(this) // this en vanilla JS
//   console.log($(this)) // this en JQuery
// })

// tabs fonctionnalités
jQuery(function ($) {
  /**
   *
   * @param {JQuery} $a Link
   * @param {number} animationDuration
   */
  var displayTab = function ($a, animationDuration) {
    if (animationDuration === undefined) {
      animationDuration = 500
    }
    var $li = $a.parent()
    if ($li.hasClass('active')) {
      return false
    }
    var $target = $($a.attr('href'))
    console.log($target)
    $li.siblings('.active').removeClass('active')
    $li.addClass('active')

    $target.siblings(':visible').fadeOut(animationDuration, function () {
      $target.fadeIn(animationDuration)
    })
  }

  $('.tabs a').click(function (e) {
    var $a = displayTab($(this))
  })

  var hash = window.location.hash
  if (hash != '') {
    var $a = $('.tabs a[href="' + hash + '"]')
    if ($a.length > 0) {
      displayTab($a, 0)
    }
  }

  // var $ul = $('#users')
  // $.get(
  //   'https://jsonplaceholder.typicode.com/users',
  //   {},
  //   function (data, textStatus, jqXHR) {
  //     data.forEach(function (user) {
  //       var $li = $('<li>')
  //       $li.text(user.name)
  //       $ul.append($li)
  //       // $li.appendTo($ul) // équivalent à l'écriture ci dessus
  //     })
  //   },
  //   'json'
  // )
  // a partir de jquer 1.?

  var $ul = $('#users')
  var $loader = $('<div>Loading...</div>').appendTo('body')
  $.get('https://jsonplaceholder.typicode.com/users')
    .done(function (data, textStatus, jqXHR) {
      data.forEach(function (user) {
        var $li = $('<li>')
        $li.text(user.name)
        $ul.append($li)
        // $li.appendTo($ul) // équivalent à l'écriture ci dessus
      })
    })
    .fail(function (jqXHR, textStatus, errorThrown) {
      console.error(errorThrown)
    })
    .always(function () {
      console.log('always')
      $loader.remove()
    })
})
