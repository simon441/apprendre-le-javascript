console.log(this)
var eleve = {
  name: 'Strati',
  present: function () {
    var self = this
    var demo = {
      demo: function () {
        console.log(self.name)
      },
    }
    demo.demo()
    console.log(this.name + ' présent')
  },
}

eleve.present()
