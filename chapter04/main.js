var demo = 'Salut'
console.log(demo)
var mary = {
  name: 'Mary',
  age: 18,
  moyenne: 15,
}

// comparaison operators: =, !=, >, >=, <, <=, !=, ===, !==
// equals, not equals, greater than, greater or equals to, lesser then, lesser or equals to, equals and same type, différent and not same type

/**
 * IF, ELSE, ELSE IF
 */

if (mary.age >= 18) {
  console.log('Tu es majeure !')
}
if (mary.age < 18) {
  console.log('Tu es mineure')
}

if (mary.age >= 18) {
  console.log('Tu es majeure !')
} else {
  console.log('Tu es mineure')
}

/*
SI age >= 18 ALORS
    Tu peux conduire
SINON SI age >= 15 ALORS
    Tu peux passer la conduite accompagnée
SINON
    Tu ne peux pas conduire
*/
mary.age = 15
if (mary.age >= 18) {
  console.log('Tu peux passer ton permis')
} else if (mary.age >= 15) {
  console.log('Tu peux passer la conduite accompagnée')
} else {
  console.log('Tu ne peux pas encore conduire')
}

/*
ET : &&
OU : ||
NON : ! (inverser le resultat de l'opération booléeene: FAUX devient VRAI et VRAI devient FAUX)

SI 18 <= age <= 15 ALORS
 bravo! tu as le droit au tarif jeune
FIN SI
 */
mary.age = 21
if (mary.age >= 18 && mary.age <= 25) {
  console.log('bravo ! tu as le droit au tarif jeune')
} else {
  console.log("Vous n'avez pas accès au tarif jeune")
}

if (mary.age < 18 || mary.age > 25) {
  console.log("Vous n'avez pas accès au tarif jeune")
} else {
  console.log('bravo ! tu as le droit au tarif jeune')
}

mary.age = 72
if ((mary.age >= 18 && mary.age <= 25) || mary.age > 70) {
  console.log('Vous avez accès au tarif réduit')
} else {
  console.log("Vous n'avez pas accès au tarif réduit")
}

mary.age = 18
// conditions d'égalité
// égalité de valeur (vrai dans le test suivant: même valeur)
if (mary.age == '18') {
  console.log('Bravo tu as 18 ans')
}
// égalité de valeur et de type (faux dans le test suivant: même valeur mais type différent)
if (mary.age === '18') {
  console.log('Bravo tu as 18 ans')
}
// égalité de valeur et de type (vrai dans le test suivant: même valeur et même type)
if (mary.age === 18) {
  console.log('Bravo tu as 18 ans')
}

/**
 * SWITCH CASE
 */
mary.age = 41
if (mary.age === 18) {
  console.log('Bravo tu as 18 ans')
} else if (mary.age === 25) {
  console.log('Bravo tu as un quart de siècle')
} else if (mary.age === 50) {
  console.log('Bravo tu as un demi siècle')
}

// equivalent to
switch (mary.age) {
  case 18:
    console.log('Bravo tu as 18 ans')
    break
  case 25:
    console.log('Bravo tu as un quart de siècle')
    break
  case 50:
    console.log('Bravo tu as un demi siècle')
    break
  default:
    console.log('Bon anniversaire')
}

/**
 * OPERATEUR TERNAIRE consition ? si_vrai :  si_faux
 */
mary.age = 12
console.log(mary.age >= 18 ? 'Tu es majeure' : 'Tu es mineure')
