// Prototype: création d'objets
// var eleve = {
//   moyenne: function () {
//     var somme = 0
//     for (i = 0; i < this.notes.length; i++) {
//       somme += this.notes[i]
//     }
//     return somme / this.notes.length
//   },
//   present: function () {
//     console.log(this.name + ' présent')
//   },
// }

// var jean = {
//   name: 'Jean',
//   notes: [10, 20],
// }

// var violet = {
//   name: 'Violette',
//   notes: [20, 19],
// }

// console.log(Object.getPrototypeOf(jean))

// première façon avec Object.create()
// var maria = Object.create(eleve)
// maria.name = 'Maria'
// maria.notes = [12]
// var m = maria.moyenne()
// m

// deuxième façon avec un constructeur
var Eleve = function (nom, notes) {
  if (notes !== undefined) {
    this.notes = notes
  }
  this.nom = nom
}

// Affecte la class Eleve
Eleve.moyenne = function () {
  return 10
}

// Affecte les instances de l'objet Eleve
Eleve.prototype.moyenne = function () {
  if (this.notes === undefined) {
    return NaN
  }
  var somme = 0
  for (i = 0; i < this.notes.length; i++) {
    somme += this.notes[i]
  }
  return somme / this.notes.length
}

var jean = new Eleve('Jean', [10, 20])
var maria = new Eleve('Maria')
console.log(jean.moyenne())
maria
