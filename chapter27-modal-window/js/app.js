/** @type {?HTMLElement} modal */
let modal = null
/** @type {string} */
const focusableSelector = 'button, a, input, textarea, select'
/** @type {HTMLElement[]} */
let focusables = []
/** @type {?HTMLElement} */
let previousFocusedElement = null
/** @type {boolean} loadAjaxModelOnlyOnce - load an outside modal only once and not refetch from the server. (do not fetch more than one = true)*/
// const loadAjaxModalOnlyOnce = true

/**
 *
 * @param {any} e
 */
const openModal = async function (e) {
  e.preventDefault()

  /** @type {string} target */
  const target = e.target.getAttribute('href')
  if (target.startsWith('#')) {
    modal = document.querySelector(target)
  } else {
    const allowReloadModal = e.target.getAttribute('data-network-once') || e.target.classList.contains('js-network-allow-reload')
    console.log(e.target.classList.contains('js-network-allow-reload'), e.target.getAttribute('data-network-once'), allowReloadModal)
    modal = await loadModal(target, allowReloadModal)
  }
  focusables = Array.from(modal.querySelectorAll(focusableSelector))
  previousFocusedElement = document.querySelector(':focus')
  focusables[0].focus()

  modal.style.display = ''
  modal.setAttribute('aria-hidden', 'false')
  modal.setAttribute('aria-modal', 'true')
  modal.addEventListener('click', closeModal)
  modal.querySelector('.js-modal-close').addEventListener('click', closeModal)
  modal.querySelector('.js-modal-stop').addEventListener('click', stopPropagation)
}

const closeModal = function (e) {
  if (modal === null) {
    return
  }
  // give back focus to the element calling the modal
  if (previousFocusedElement !== null) {
    previousFocusedElement.focus()
  }
  e.preventDefault()

  // for a reversed animation
  /*modal.style.display = 'none'
  modal.offsetWidth
  modal.style.display = ''*/

  // delay the display none to give the time for the CSS animation
  const hideModal = function () {
    modal.style.display = 'none'
    modal.removeEventListener('animationend', hideModal)
    modal = null
  }
  modal.addEventListener('animationend', hideModal)

  modal.setAttribute('aria-hidden', 'true')
  modal.removeAttribute('aria-modal')
  modal.removeEventListener('click', closeModal)
  modal.querySelector('.js-modal-stop').removeEventListener('click', stopPropagation)
  modal.querySelector('.js-modal-close').removeEventListener('click', closeModal)
}

/**
 * Stops the modal from closing on click inside modal
 * @param {*} e
 */
const stopPropagation = function (e) {
  e.stopPropagation()
}

/**
 * Parse the requested url
 * @param {string} url
 * @param {boolean} allowReloadModal - Reload from the network even if already fetched? (default=false)
 * @return {Promise}
 */
const loadModal = async function (url, allowReloadModal = false) {
  const target = '#' + url.split('#')[1]
  if (allowReloadModal !== true) {
    const existingModal = document.querySelector(target)
    if (existingModal !== null) {
      return existingModal
    }
  }
  // add a loader
  const loader = document.createElement('div')
  loader.className = 'js-modal-loader'
  loader.innerHTML = '<p>Loading...</p>'
  document.body.appendChild(loader)

  const html = await fetch(url).then(res => res.text())

  loader.parentNode.removeChild(loader)
  const element = document.createRange().createContextualFragment(html).querySelector(target)
  if (element === null) {
    throw new Error(`The element ${target} was not found in the page ${url}`)
  }
  document.body.append(element)
  return element
}

/**
 *  Keybord management to keep Tab key navigation inside the modal only
 * loop through the focusable elements
 * @param {KeyboardEvent} e
 */
const focusInModal = function (e) {
  e.preventDefault()
  let index = focusables.findIndex(f => f === modal.querySelector(':focus'))
  // backwards or forwards on shift + Tab or Tab
  if (e.shiftKey === true) {
    index--
  } else {
    index++
  }
  // if last element go back to first
  if (index >= focusables.length) {
    index = 0
  }
  // if first element go to last
  if (index < 0) {
    index = focusables.length - 1
  }

  focusables[index].focus()
}

/**
 * @param {HTMLAnchorElement} a
 */
document.querySelectorAll('.js-modal').forEach(a => {
  a.addEventListener('click', openModal)
})

window.addEventListener('keydown', function (e) {
  if (e.key === 'Escape' || e.key === 'esc') {
    closeModal(e)
  }

  if (e.key === 'Tab' && modal !== null) {
    focusInModal(e)
  }
})
