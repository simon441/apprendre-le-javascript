;(function () {
  /*
   * LORSQUE l'on clique sur un onglet
   *      ON RETIRE la classe .active de l'onglet actuel
   *      J'AJOUTE la classe .active à l'onglet actuel
   *
   *      ON RETIRE la class .active sur le contenu actif
   *      j'ajoute la class .active sur le contenu correspondant à mon clic
   */
  /**
   *
   * @param {HTMLAnchorElement} a
   * @param {Boolean} displayAnimations
   */
  var displayTab = function (a, displayAnimations) {
    if (displayAnimations === undefined) {
      displayAnimations = true
    }
    // variable declaration
    /**
     * @type {HTMLLIElement}
     */
    var li = a.parentNode
    /**
     * @type {HTMLDivElement}
     */
    var div = a.parentNode.parentNode.parentNode
    /** @type {HTMLDivElement} Contenu actif */
    var activeTab = div.querySelector('.tab-content.active')
    /** @type {HTMLAnchorElement} Contenu à afficher */
    var target = div.querySelector(a.getAttribute('href'))

    // ne rien faire si click sur onglet actif
    if (li.classList.contains('active')) {
      return false
    }

    // ON RETIRE la classe .active de l'onglet actuel
    div.querySelector('.tabs .active').classList.remove('active')
    // J'AJOUTE la classe .active à l'onglet actuel
    li.classList.add('active')

    // On ajoute la classe .fade sur l'élément actif
    // A la fin de l'animation
    //    On retire la classe .fade et .active
    //    On ajoute la classe .active et .fade  à l'élément affiché
    //    On ajoute la classe .in
    if (displayAnimations) {
      activeTab.classList.add('fade')
      activeTab.classList.remove('in')
      var transitionend = function () {
        this.classList.remove('fade')
        this.classList.remove('active')
        target.classList.add('active')
        target.classList.add('fade')
        target.offsetWidth
        target.classList.add('in')
        activeTab.removeEventListener('transitionend', transitionend)
        activeTab.removeEventListener('oTransitionEnd', transitionend)
        activeTab.removeEventListener('webkitTransitionEnd', transitionend)
      }
      activeTab.addEventListener('transitionend', transitionend)
      activeTab.addEventListener('oTransitionEnd', transitionend)
      activeTab.addEventListener(' webkitTransitionEnd', transitionend)
    } else {
      // j'ajoute la class .active sur le contenu correspondant à mon clic
      target.classList.add('active')
      // ON RETIRE la class .active sur le contenu actif
      activeTab.classList.remove('active')
    }
  }

  var tabs = document.querySelectorAll('.tabs a')
  for (var i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', function (e) {
      displayTab(this)
    })
  }

  /**
   * JE RECUPERE le hash
   * AJOUTER LA CLASSE .active sur le lien qui a comme href le hash: href=hash"
   * RETIRER LA CLASSE .active sur les autres onglets
   * AFFICHER / Masquer les contenus
   */

  var hashChange = function (e) {
    var hash = window.location.hash
    var a = document.querySelector('a[href="' + hash + '"]')
    if (a !== null && !a.classList.contains('active')) {
      displayTab(a, e !== undefined)
    }
  }
  window.addEventListener('hashchange', hashChange)
  hashChange()
})()
