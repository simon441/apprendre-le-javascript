<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/app.js" defer></script>
</head>

<body>
    <nav>
        <?php for ($i = 1; $i <= 4; $i++) : ?>
            <a href="#section<?= $i; ?>">Section <?= $i; ?></a>
        <?php endfor; ?>
        <a href="#">Contact</a>
    </nav>

    <main>
        <?php for ($i = 1; $i <= 4; $i++) : ?>
            <section id="section<?= $i; ?>" data-spy>
                Section <?= $i; ?>
            </section>
        <?php endfor; ?>
        <a href="#">Contact</a>
    </main>
    <hr>
</body>

</html>