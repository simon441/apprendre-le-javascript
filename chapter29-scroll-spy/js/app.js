/** @type {number} ratio - ratio of page visible on screen for observer */
const ratio = 0.6
/** @type {?IntersectionObserver} currentObserver - persit the current observer */
let currentObserver = null

/** @type {NodeListOf<HTMLElement>} */
const spies = document.querySelectorAll('[data-spy]')

/**
 * Activate an element
 * @param {Element} element
 * @returns {void|null}
 */
const activateElement = function (element) {
  const id = element.getAttribute('id')
  const anchor = document.querySelector(`a[href="#${id}"]`)
  if (anchor === null) {
    return null
  }
  // remove active from all elements before activating the current one
  anchor.parentElement.querySelectorAll('.active').forEach(node => node.classList.remove('active'))
  anchor.classList.add('active')
}

/**
 * Callback for IntersectionObserver
 * @param {IntersectionObserverEntry[]} entries
 */
const callback = function (entries) {
  entries.forEach(entry => {
    if (entry.isIntersecting) {
      activateElement(entry.target)
    }
  })
}

/**
 * Spying on the elements
 * @param {NodeListOf<HTMLElement>} elements
 */
const observe = function (elements) {
  if (currentObserver !== null) {
    elements.forEach(element => currentObserver.unobserve(element))
  }

  const y = Math.round(window.innerHeight * ratio)

  /** @type {IntersectionObserver} */
  currentObserver = new IntersectionObserver(callback, {
    rootMargin: `-${window.innerHeight - y - 1}px 0px -${y}px 0px`, // limit the root margin to a 1 pixel line
  })

  elements.forEach(element => currentObserver.observe(element))
}

/**
 * Debounce
 * @param {Function} callback
 * @param {number} delay
 * @returns {any|Function}
 */
function debounce(callback, delay) {
  let timer
  return function () {
    let args = arguments
    let context = this
    clearTimeout(timer)
    timer = setTimeout(function () {
      callback.apply(context, args)
    }, delay)
  }
}

if (spies.length > 0) {
  observe(spies)
  let windowHeight = window.innerHeight
  window.addEventListener(
    'resize',
    debounce(function () {
      if (window.innerHeight !== windowHeight) {
        observe(spies)
        windowHeight = window.innerHeight
      }
    }, 500)
  )
}
