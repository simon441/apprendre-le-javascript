// L'objet Window
;(function () {
  //   console.log(window)
  /* methodes courantes de l'ojet window */
  // window.alert('Alerte')
  //   var demo = window.prompt('Salut les gens')
  //   console.log(demo)
  //   var confirmation = window.confirm('Confirmez-vous?')
  /* 
    EXERCICE: deviner un chiffre

    générer un chiffre entre 0 et 10
    si l'utilisateur devine le chiffre on dit Bravo!
    si l'utilisateur ne devine pas le chiffre
        si il est au dessus: Tu es au dessus
        si il est en dessous: Tu es en dessous
    ensuite, on lui redemande de rentrer un chiffre
    hint: utiliser le prompt et le alert

    // algorithme
    ON choisit un chiffre RANDOM
    DEMANDE à l'utilisateur de rentrer un chiffre
    TANT QUE le chiffre n'est pas bon
        SI le chiffre est bon ALORS
            ALERT Bravo
        SINON
            SI le chiffre est au dessus ALORS
                ALERT Au dessus
            SINON
                ALERT En dessous
            DEMANDE à l'utilisateur de rentrer un chiffre
    FIN TANT QUE
    ALERT succès
    */
  /*  var aDeviner = Math.round(Math.random() * 10)
  var essai = parseInt(window.prompt('Entrer votre chiffre entre 0 et 10'), 10)
  var nombreEssais = 3 // nombre d'essais maximum possible

  while (essai !== aDeviner && nombreEssais > 0) {
    nombreEssais--
    if (essai > aDeviner) {
      window.alert('Au dessus !')
    } else {
      window.alert('En dessous !')
    }
    if (nombreEssais > 0) {
      var essai = parseInt(window.prompt('Retentez votre chance'), 10)
    }
  }
  if (essai === aDeviner) {
    window.alert('Bravo !')
  } else {
    window.alert('Echec')
  }

  // autre solution
  var aDeviner = Math.round(Math.random() * 10)
  var essai
  var nombreEssais = 3 // nombre d'essais maximum possible

  for (var i = 0; i < nombreEssais; i++) {
    if (i === 0) {
      essai = window.prompt('Entrer votre chiffre entre 0 et 10')
    } else {
      essai = window.prompt('Retentez votre chance')
    }
    essai = parseInt(essai, 10)
    if (essai === aDeviner) {
      break
    }
    if (essai > aDeviner) {
      window.alert('Trop haut !')
    } else {
      window.alert('Trop bas !')
    }
  }
  //    (essai !== aDeviner && nombreEssais > 0) {
  //     nombreEssais--
  //     if (nombreEssais > 0) {
  //       var essai = parseInt(window.prompt('Retentez votre chance'), 10)
  //     }
  //   }
  if (essai === aDeviner) {
    window.alert('Bravo')
  } else {
    window.alert('Echec')
  }*/
  /* TIMERS */
  //   var demo = function () {
  //     console.log('demo')
  //   }
  //   var i = 0
  //   var timer = window.setInterval(function () {
  //     i++
  //     if (i === 10) {
  //       window.clearInterval(timer)
  //     }
  //     console.log(i)
  //   }, 1000)
  //   for (var i = 0; i < 10; i++) {
  //     ;(function (i) {
  //       window.setTimeout(function () {
  //         console.log(i)
  //       }, 1000)
  //     })(i)
  //   }
})()
