// @ts-check
// if (typeof Object.assign !== 'function') {
//   // Must be writable: true, enumerable: false, configurable: true
//   Object.defineProperty(Object, 'assign', {
//     value: function assign(target, varArgs) {
//       // .length of function is 2
//       'use strict'
//       if (target === null || target === undefined) {
//         throw new TypeError('Cannot convert undefined or null to object')
//       }

//       var to = Object(target)

//       for (var index = 1; index < arguments.length; index++) {
//         var nextSource = arguments[index]

//         if (nextSource !== null && nextSource !== undefined) {
//           for (var nextKey in nextSource) {
//             // Avoid bugs when hasOwnProperty is shadowed
//             if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
//               to[nextKey] = nextSource[nextKey]
//             }
//           }
//         }
//       }
//       return to
//     },
//     writable: true,
//     configurable: true,
//   })
// }
/**
 * @typedef CarouselOptions
 * @type {Object}
 * @property {number|1} options.slidesToScroll Nombre d'éléments à faire défiler
 * @property {number|1} options.slidesVisible Nombre d'éléments visibles dans un slide
 * @property {boolean|false} options.loop Doit-on boucler en fin de Carousel
 * @property {boolean|false} options.pagination Montrer la pagination
 * @property {boolean|true} options.navigation Montrer la navigation à la souris
 */

class Carousel {
  static CSS_PAGINATION_ACTIVE = 'carousel__pagination__button--active'

  /**
   * This callback is used to be added to onMove
   * @callback moveCallback
   * @param {number} index
   */

  /**
   *
   * @param {HTMLElement} element
   * @param {Object} options
   * @param {number} [options.slidesToScroll=1] Nombre d'éléments à faire défiler
   * @param {number} [options.slidesVisible=1] Nombre d'éléments visibles dans un slide
   * @param {boolean} [options.loop=false] Doit-on boucler en fin de Carousel
   * @param {boolean} [options.pagination=false]
   * @param {boolean} [options.navigation=true]
   */
  constructor(element, options = {}) {
    this.element = element
    /** @type {CarouselOptions} */
    this.options = Object.assign(
      {},
      {
        slidesToScroll: 1,
        slidesVisible: 1,
        loop: false,
        pagination: false,
        navigation: true,
      },
      options
    )

    /** @type {moveCallback[]} list of callback for the navigation */
    this.moveCallbacks = []
    /**
     * @type {boolean} - for responsive
     */
    this.isMobile = false

    /** @type {Array} - items for the carousel in the DOM at the start */
    const children = [].slice.call(element.children)

    /** @type {number} - current active item in the carousel */
    this.currentItem = 0

    // structure HTML: modification du DOM
    /** @type {HTMLDivElement} root node  */
    this.root = this.createDivWithClass('carousel')
    /** @type {HTMLDivElement} container node */
    this.container = this.createDivWithClass('carousel__container')

    this.root.setAttribute('tabindex', '0')
    this.root.appendChild(this.container)
    this.element.appendChild(this.root)

    /** @type {HTMLElement[]} place tous les enfants dans le carousel container */
    this.items = children.map(child => {
      const item = this.createDivWithClass('carousel__item')

      item.appendChild(child)
      this.container.appendChild(item)
      return item
    })
    this.setStyle()
    if (this.options.navigation === true) {
      this.createNavigation()
    }
    if (this.options.pagination === true) {
      this.createPagination()
    }

    // évènements
    this.moveCallbacks.forEach(cb => cb(0))
    this.onWindowResize()
    window.addEventListener('resize', this.onWindowResize)
    this.root.addEventListener('keyup', e => {
      if (e.key === 'ArrowRight' || e.key === 'Right' || e.keyCode === 39) {
        this.next()
      } else if (e.key === 'ArrowLeft' || e.key === 'Left' || e.keyCode === 37) {
        this.prev()
      }
    })
  }

  /**
   * Applique les bonnes dimensions aux éléments du carousel
   */
  setStyle() {
    const ratio = this.items.length / this.slidesVisible
    this.container.style.width = ratio * 100 + '%'
    this.items.forEach(item => (item.style.width = 100 / this.slidesVisible / ratio + '%'))
  }

  /**
   * crée les flèches de navigation
   */
  createNavigation() {
    const nextButton = this.createDivWithClass('carousel__next')
    const prevButton = this.createDivWithClass('carousel__prev')
    this.root.appendChild(nextButton)
    this.root.appendChild(prevButton)
    nextButton.addEventListener('click', this.next.bind(this))
    prevButton.addEventListener('click', this.prev.bind(this))
    if (this.options.loop === true) {
      return
    }
    this.onMove(index => {
      if (index === 0) {
        prevButton.classList.add('carousel__prev--hidden')
      } else {
        prevButton.classList.remove('carousel__prev--hidden')
      }
      if (this.items[this.currentItem + this.slidesVisible] === undefined) {
        nextButton.classList.add('carousel__next--hidden')
      } else {
        nextButton.classList.remove('carousel__next--hidden')
      }
    })
  }

  /**
   * crée une pagination
   */
  createPagination() {
    const pagination = this.createDivWithClass('carousel__pagination')
    /** @type {HTMLDivElement[]} pagination buttons */
    const buttons = []

    this.root.appendChild(pagination)

    for (let i = 0; i < this.items.length; i = i + this.options.slidesToScroll) {
      const button = this.createDivWithClass('carousel__pagination__button')
      button.addEventListener('click', () => this.goToItem(i))
      pagination.appendChild(button)
      buttons.push(button)
    }

    this.onMove(index => {
      const activeButton = buttons[Math.floor(index / this.options.slidesToScroll)]
      if (activeButton) {
        buttons.forEach(button => button.classList.remove('carousel__pagination__button--active'))
        activeButton.classList.add('carousel__pagination__button--active')
      }
    })
  }

  /**
   * listener pour le bouton suivant
   */
  next = () => {
    this.goToItem(this.currentItem + this.slidesToScroll)
  }

  /**
   * listener pour le bouton précédent
   */
  prev = () => {
    this.goToItem(this.currentItem - this.slidesToScroll)
  }

  /**
   * Déplace le Carousel vers l'élément ciblé
   * @param {number} index
   */
  goToItem(index) {
    if (index < 0) {
      if (this.options.loop) {
        index = this.items.length - this.slidesVisible
      } else {
        return
      }
    } else if (
      index >= this.items.length ||
      (this.items[this.currentItem + this.slidesVisible] === undefined && index > this.currentItem)
    ) {
      if (this.options.loop) {
        index = 0
      } else {
        return
      }
    }
    const translateX = (index * -100) / this.items.length
    this.container.style.transform = 'translate3d(' + translateX + '%, 0, 0)'
    this.currentItem = index

    this.moveCallbacks.forEach(cb => cb(index))
  }

  /**
   *
   * @param {moveCallback} cb
   */
  onMove(cb) {
    this.moveCallbacks.push(cb)
  }

  onWindowResize = () => {
    const mobile = window.innerWidth < 600
    if (mobile !== this.isMobile) {
      this.isMobile = mobile
      this.setStyle()
      this.moveCallbacks.forEach(cb => cb(this.currentItem))
    }
  }

  /**
   * Crée une nouvelle div, puis y rajoute une classe
   * @param {string} className
   * @returns {HTMLDivElement}
   */
  createDivWithClass(className) {
    const div = document.createElement('div')
    div.setAttribute('class', className)
    return div
  }

  /**
   * @returns {number}
   */
  get slidesToScroll() {
    return this.isMobile ? 1 : this.options.slidesToScroll
  }

  /**
   * @returns {number}
   */
  get slidesVisible() {
    return this.isMobile ? 1 : this.options.slidesToScroll
  }
}
const onReady = function () {
  new Carousel(document.querySelector('#carousel1'), {
    slidesToScroll: 3,
    slidesVisible: 3,
    loop: true,
  })

  new Carousel(document.querySelector('#carousel2'), {
    slidesToScroll: 2,
    slidesVisible: 2,
    pagination: true,
    loop: true,
  })

  new Carousel(document.querySelector('#carousel3'))
}
if (document.readyState !== 'loading') {
  onReady()
}
document.addEventListener('DOMContentLoaded', onReady)
