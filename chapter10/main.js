// Gestion des erreurs
var a = {}

try {
  a.maMethode()
} catch (e) {
  console.error("J'ai rencontré une erreur " + e.stack)
} finally {
  console.log('finally')
}

var double = function (nombre) {
  var resultat = nombre * 2
  if (Number.isNaN(resultat)) {
    throw new Error("Le nombre n'est pas un chiffre")
  }
  return resultat
}

try {
  var a = 2
  console.log(double('aze'))
} catch (error) {
  console.error('Impossible de multiplier aze par 2')
}

var demo = function (nombre) {
  if (nombre > 5) {
    throw new Error('Le nombre ne doit pas être supérieur à 5')
  }
  return nombre * 2
}

demo(6)
try {
  try {
    throw new Error('erreur')
  } finally {
    console.log('finally')
  }
} catch (error) {
  console.log('catch')
} finally {
}
