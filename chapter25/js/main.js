// @ts-check
// if (typeof Object.assign !== 'function') {
//   // Must be writable: true, enumerable: false, configurable: true
//   Object.defineProperty(Object, 'assign', {
//     value: function assign(target, varArgs) {
//       // .length of function is 2
//       'use strict'
//       if (target === null || target === undefined) {
//         throw new TypeError('Cannot convert undefined or null to object')
//       }

//       var to = Object(target)

//       for (var index = 1; index < arguments.length; index++) {
//         var nextSource = arguments[index]

//         if (nextSource !== null && nextSource !== undefined) {
//           for (var nextKey in nextSource) {
//             // Avoid bugs when hasOwnProperty is shadowed
//             if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
//               to[nextKey] = nextSource[nextKey]
//             }
//           }
//         }
//       }
//       return to
//     },
//     writable: true,
//     configurable: true,
//   })
// }
/**
 * @typedef CarouselOptions
 * @type {Object}
 * @property {number|1} options.slidesToScroll Nombre d'éléments à faire défiler
 * @property {number|1} options.slidesVisible Nombre d'éléments visibles dans un slide
 * @property {boolean|false} options.loop Doit-on boucler en fin de Carousel
 * @property {boolean|false} options.infinite Défilement infini
 * @property {boolean|false} options.pagination Montrer la pagination
 * @property {boolean|true} options.navigation Montrer la navigation à la souris
 */

class Carousel {
  static CSS_PAGINATION_ACTIVE = 'carousel__pagination__button--active'

  /**
   * This callback is used to be added to onMove
   * @callback moveCallback
   * @param {number} index
   */

  /**
   *
   * @param {HTMLElement} element
   * @param {Object} options
   * @param {number} [options.slidesToScroll=1] Nombre d'éléments à faire défiler
   * @param {number} [options.slidesVisible=1] Nombre d'éléments visibles dans un slide
   * @param {boolean} [options.loop=false] Doit-on boucler en fin de Carousel
   * @param {boolean} [options.infinite=false] Défilement infini
   * @param {boolean} [options.pagination=false]
   * @param {boolean} [options.navigation=true]
   */
  constructor(element, options = {}) {
    this.element = element
    /** @type {CarouselOptions} */
    this.options = Object.assign(
      {},
      {
        slidesToScroll: 1,
        slidesVisible: 1,
        loop: false,
        pagination: false,
        navigation: true,
        infinite: false,
      },
      options
    )

    if (this.options.loop && this.options.infinite) {
      throw new Error('Un carousel ne peut être à la fois en boucle et en infini')
    }

    /** @type {moveCallback[]} list of callback for the navigation */
    this.moveCallbacks = []
    /**
     * @type {boolean} - for responsive
     */
    this.isMobile = false

    /** @type {Node[]} - items for the carousel in the DOM at the start */
    const children = [].slice.call(element.children)

    /** @type {number} - current active item in the carousel */
    this.currentItem = 0

    /** @type {number} Offset du nombre d'éléments (combien d'élément à décaler) */
    this.offset = 0

    // structure HTML: modification du DOM
    /** @type {HTMLDivElement} root node  */
    this.root = this.createDivWithClass('carousel')
    /** @type {HTMLDivElement} container node */
    this.container = this.createDivWithClass('carousel__container')

    this.root.setAttribute('tabindex', '0')
    this.root.appendChild(this.container)
    this.element.appendChild(this.root)

    /** @type {HTMLDivElement[]} tous les enfants de l'élément carousel dans le carousel container */
    this.items = children.map(child => {
      const item = this.createDivWithClass('carousel__item')

      item.appendChild(child)
      return item
    })

    if (this.options.infinite) {
      this.offset = this.slidesVisible + this.options.slidesToScroll
      if (this.offset > children.length) {
        console.error("Vous n'avez pas assez d'éléments dans le carousel", element)
      }

      // @ts-ignore
      this.items = [
        ...this.items.slice(this.items.length - this.offset).map(item => item.cloneNode(true)),
        ...this.items,
        ...this.items.slice(0, this.offset).map(item => item.cloneNode(true)),
      ]
      this.goToItem(this.offset, false)
    }

    this.items.forEach(item => {
      this.container.appendChild(item)
    })

    this.setStyle()
    if (this.options.navigation === true) {
      this.createNavigation()
    }
    if (this.options.pagination === true) {
      this.createPagination()
    }

    // évènements
    this.moveCallbacks.forEach(cb => cb(this.currentItem))
    this.onWindowResize()
    window.addEventListener('resize', this.onWindowResize)
    this.root.addEventListener('keyup', e => {
      if (e.key === 'ArrowRight' || e.key === 'Right' || e.keyCode === 39) {
        this.next()
      } else if (e.key === 'ArrowLeft' || e.key === 'Left' || e.keyCode === 37) {
        this.prev()
      }
    })

    if (this.options.infinite) {
      this.container.addEventListener('transitionend', this.resetInfinite)
      this.container.addEventListener('webkitTransitionEnd', this.resetInfinite)
      this.container.addEventListener('oTransitionEnd', this.resetInfinite)
    }
  }

  /**
   * Applique les bonnes dimensions aux éléments du carousel
   */
  setStyle() {
    const ratio = this.items.length / this.slidesVisible
    this.container.style.width = ratio * 100 + '%'
    this.items.forEach(item => (item.style.width = 100 / this.slidesVisible / ratio + '%'))
  }

  /**
   * crée les flèches de navigation
   */
  createNavigation() {
    const nextButton = this.createDivWithClass('carousel__next')
    const prevButton = this.createDivWithClass('carousel__prev')
    this.root.appendChild(nextButton)
    this.root.appendChild(prevButton)
    nextButton.addEventListener('click', this.next.bind(this))
    prevButton.addEventListener('click', this.prev.bind(this))
    if (this.options.loop === true) {
      return
    }
    this.onMove(index => {
      if (index === 0) {
        prevButton.classList.add('carousel__prev--hidden')
      } else {
        prevButton.classList.remove('carousel__prev--hidden')
      }
      if (this.items[this.currentItem + this.slidesVisible] === undefined) {
        nextButton.classList.add('carousel__next--hidden')
      } else {
        nextButton.classList.remove('carousel__next--hidden')
      }
    })
  }

  /**
   * crée une pagination
   */
  createPagination() {
    const pagination = this.createDivWithClass('carousel__pagination')
    /** @type {HTMLDivElement[]} pagination buttons */
    const buttons = []

    this.root.appendChild(pagination)

    for (let i = 0; i < this.items.length - 2 * this.offset; i = i + this.options.slidesToScroll) {
      const button = this.createDivWithClass('carousel__pagination__button')
      button.addEventListener('click', () => this.goToItem(i + this.offset))
      pagination.appendChild(button)
      buttons.push(button)
    }

    this.onMove(index => {
      /** @type {number} nombre d'éléments */
      const count = this.items.length - 2 * this.offset
      const activeButton = buttons[Math.floor(((index - this.offset) % count) / this.options.slidesToScroll)]
      if (activeButton) {
        buttons.forEach(button => button.classList.remove('carousel__pagination__button--active'))
        activeButton.classList.add('carousel__pagination__button--active')
      }
    })
  }

  /**
   * listener pour le bouton suivant
   */
  next = () => {
    this.goToItem(this.currentItem + this.slidesToScroll)
  }

  /**
   * listener pour le bouton précédent
   */
  prev = () => {
    this.goToItem(this.currentItem - this.slidesToScroll)
  }

  /**
   * Déplace le Carousel vers l'élément ciblé
   * @param {number} index
   * @param {boolean} [animation=true] Effectuer l'effet de transition si true
   */
  goToItem(index, animation = true) {
    if (index < 0) {
      if (this.options.loop) {
        index = this.items.length - this.slidesVisible
      } else {
        return
      }
    } else if (
      index >= this.items.length ||
      (this.items[this.currentItem + this.slidesVisible] === undefined && index > this.currentItem)
    ) {
      if (this.options.loop) {
        index = 0
      } else {
        return
      }
    }
    const translateX = (index * -100) / this.items.length
    if (animation === false) {
      this.container.style.transition = 'none'
    }
    this.container.style.transform = 'translate3d(' + translateX + '%, 0, 0)'
    this.container.offsetHeight // Force le repaint
    if (animation === false) {
      this.container.style.transition = ''
    }
    this.currentItem = index

    this.moveCallbacks.forEach(cb => cb(index))
  }

  /**
   * Déplace le container pour donner l'impression d'un slide infini
   */
  resetInfinite = () => {
    if (this.currentItem <= this.options.slidesToScroll) {
      this.goToItem(this.currentItem + (this.items.length - 2 * this.offset), false)
    } else if (this.currentItem >= this.items.length - this.offset) {
      this.goToItem(this.currentItem - (this.items.length - 2 * this.offset), false)
    }
  }

  /**
   *
   * @param {moveCallback} cb
   */
  onMove(cb) {
    this.moveCallbacks.push(cb)
  }

  onWindowResize = () => {
    const mobile = window.innerWidth < 600
    if (mobile !== this.isMobile) {
      this.isMobile = mobile
      this.setStyle()
      this.moveCallbacks.forEach(cb => cb(this.currentItem))
    }
  }

  /**
   * Crée une nouvelle div, puis y rajoute une classe
   * @param {string} className
   * @returns {HTMLDivElement}
   */
  createDivWithClass(className) {
    const div = document.createElement('div')
    div.setAttribute('class', className)
    return div
  }

  /**
   * @returns {number}
   */
  get slidesToScroll() {
    return this.isMobile ? 1 : this.options.slidesToScroll
  }

  /**
   * @returns {number}
   */
  get slidesVisible() {
    return this.isMobile ? 1 : this.options.slidesToScroll
  }
}

const onReady = function () {
  // new Carousel(document.querySelector('#carousel1'), {
  //   slidesToScroll: 2,
  //   slidesVisible: 3,
  //   loop: true,
  //   pagination: true,
  // })
  new Carousel(document.querySelector('#carousel1'), {
    slidesVisible: 3,
    slidesToScroll: 2,
    pagination: !0,
  }),
    //   // new Carousel(document.querySelector('#carousel2'), {
    //   //   slidesToScroll: 2,
    //   //   slidesVisible: 2,
    //   //   pagination: true,
    //   //   infinite: true,
    //   // })

    //   new Carousel(document.querySelector('#carousel2'), {
    //     slidesToScroll: 2,
    //     slidesVisible: 3,
    //     infinite: true,
    //     pagination: true,
    //   })

    // new Carousel(document.querySelector('#carousel3'))
    // new Carousel(document.querySelector('#carousel4'))
    new Carousel(document.querySelector('#carousel2'), {
      slidesVisible: 3,
      slidesToScroll: 3,
      loop: !0,
      pagination: !0,
    }),
    new Carousel(document.querySelector('#carousel3'), {
      slidesVisible: 3,
      slidesToScroll: 2,
      infinite: !0,
      pagination: !0,
    }),
    new Carousel(document.querySelector('#carousel4'), {
      slidesVisible: 4,
      slidesToScroll: 2,
      infinite: !0,
      pagination: !1,
    })
}
// if (document.readyState === 'complete' || (document.readyState !== 'loading' && !document.documentElement.doScroll)) {
//   // Handle it asynchronously to allow scripts the opportunity to delay ready
//   window.setTimeout(jQuery.ready)
// } else {
//   // Use the handy event callback
//   document.addEventListener('DOMContentLoaded', completed)

//   // A fallback to window.onload, that will always work
//   window.addEventListener('load', completed)
// }

const completed = function () {
  document.removeEventListener('DOMContentLoaded', completed)
  onReady()
}
// if (document.readyState !== 'loading') {
if (document.readyState === 'complete' || (document.readyState !== 'loading' && !document.documentElement.doScroll)) {
  onReady()
} else {
  document.addEventListener('DOMContentLoaded', completed)
}
// document.addEventListener('DOMContentLoaded', completed)
