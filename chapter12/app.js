// Le DOM
// var demo = document.getElementById('demo')
// if (demo.textContent) {
//   demo.textContent = 'Salut'
// } else {
//   demo.innerText = 'Salut'
// }

// make <p> tag with the class .paragraph switch class at a defined interval
// var p = document.querySelector('.paragraph')
// window.setInterval(function () {
//   p.classList.toggle('red')
// }, 1000)

// make <p> tags add class at a difined interval
// var ps = document.querySelectorAll('p')
// for (var i = 0; i < ps.length; i++) {
//   ;(function (p) {
//     window.setInterval(function () {
//       p.classList.toggle('red')
//     }, 1000)
//   })(ps[i])
// }

// work with the <li> tags
var li = document.querySelector('li') // select the first li
// li.parentElement.removeChild(li) // remove the current element

// create an element
// var li2 = document.createElement('li')
// li2.textContent = '7'
// li.parentNode.appendChild(li2)

// // replace li by li2
// li.parentElement.replaceChild(li2, li)

// insérer avant
var last = document.querySelector('ul').lastElementChild
li.parentElement.insertBefore(last, li) // nouvel élément, ancien élément
