;(function () {
  /**
   * LORSQUE JE clique sur le bouton dans la classe .spoiler
   * ALORS
   *  J'ajoute la classe .visible à l'élément suivant
   */
  /*
var button = document.querySelector('.spoiler button')
button.addEventListener('click', function () {
  this.nextElementSibling.classList.add('visible')
  this.parentNode.removeChild(this)
})
*/
  var elements = document.querySelectorAll('.spoiler')

  var createSpoilerButton = function (element) {
    // Création de la span.spoiler-content
    var span = document.createElement('span')
    span.className = 'spoiler-content'
    span.innerHTML = element.innerHTML

    // Création du bouton
    var button = document.createElement('button')
    button.textContent = 'Afficher le spoiler'

    // Ajout des éléments au DOM
    element.innerHTML = ''
    element.appendChild(button)
    element.appendChild(span)

    // On met l'écouter au click
    button.addEventListener('click', function () {
      button.parentNode.removeChild(button)
      span.classList.add('visible')
    })
  }

  for (var i = 0; i < elements.length; i++) {
    createSpoilerButton(elements[i])
  }
})()
