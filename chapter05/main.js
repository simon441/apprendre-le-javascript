// boucles
// incrémenter: i = i + 1 équivalent à i += 1 équivalent à i++
// décrémenter: i = i - 1 équivalent à i -= 1 équivalent à i--
/*
 * TANT QUE condition est vraie FAIRE
      actions
   FIN
   utilisé si one ne connait pas le nombre d'itérations à effectuer
 */
// pour sortie d'une boucle, utiliser le mot clé : break
var i = 0
while (i < 11) {
  console.log(i)
  if (i == 3) {
    // sortir de la boucle quand i atteint trois
    break
  }
  i++ // i+=1 <==> i = i+1 <==> i++
}

/**
 * POUR valeur initiale; condition; pas
 * utilisée notamment pour parcourir un tableau
 */
for (var i = 0; i < 11; i++) {
  console.log(i)
}

// parcourir un tableau
var eleves = ['Jean', 'Marc', 'Marion', 'Sylvie']

// parcourir dans l'ordre (du premier au dernier)
for (var i = 0; i < eleves.length; i++) {
  console.log('Bonjour ' + eleves[i])
}

// parcourir dans l'ordre inverse (du dernier au premier)
for (var i = eleves.length - 1; i >= 0; i--) {
  console.log('Bonjour ' + eleves[i])
}

/*
EXERCICE
var nombre = 91
est-ce que ce nombre est premier ou non?
% : modulo. permet de récupérer le reste d'une division

nombre EST PREMIER SI
  il n'est divisible que par 1 ou lui-même
  if n'est pas premier si il est divisible par un nombre compris entre deux et (lui-même -1)
*/
var nombre = 91

var i = 2
while (i < 1 + nombre / 2) {
  if (nombre % i === 0) {
    break
  }
  i++
}
if (nombre % i !== 0) {
  console.log(nombre + ' est un nombre premier')
} else {
  console.log(nombre + " n'est pas un nombre premier")
}

// CORRECTION
var nombre = 91 // pas premier
// var nombre = 97 // premier
var estPremier = true

for (var i = 2; i < nombre; i++) {
  if (nombre % i === 0) {
    estPremier = false
    console.log(nombre + " n'est pas un nombre premier")
    console.log(nombre + ' est divisible par ' + i)
    break
  }
}
if (estPremier) {
  console.log(nombre + ' est premier')
}

/*
EXERCICE 2: execice élèves
afficher les élèves qui ont en dessous de la moyenne (10)
SI moyenne < 10 ALORS
  AFFICHER nom n'as pas la moyenne
*/

var eleves = [
  {
    nom: 'Marc',
    moyenne: 15,
  },
  {
    nom: 'Marion',
    moyenne: 10,
  },
  {
    nom: 'Antoine',
    moyenne: 4,
  },
  {
    nom: 'Sophia',
    moyenne: 14,
  },
]

for (var i = 0; i < eleves.length; i++) {
  var eleve = eleves[i]
  if (eleve.moyenne < 10) {
    console.log(eleve.nom + " n'a pas la moyenne")
  } else {
    console.log(eleve.nom + ' a la moyenne')
  }
}
